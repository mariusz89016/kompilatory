class Node(object):
    pass


class List(Node):
    def __init__(self):
        self.list = []

    def add(self, element):
        self.list.append(element)
        return self


class Program(Node):
    def __init__(self, declarations, fundefs, instructions):
        self.declarations = declarations
        self.fundefs = fundefs
        self.instructions = instructions


class Declaration(Node):
    def __init__(self, type, inits):
        self.type = type
        self.inits = inits


class Init(Node):
    def __init__(self, id, expression):
        self.id = id
        self.expression = expression


class PrintInstruction(Node):
    def __init__(self, expression):
        self.expression = expression


class LabeledInstruction(Node):
    def __init__(self, id, instruction):
        self.id = id
        self.instruction = instruction


class Assignment(Node):
    def __init__(self, id, expression):
        self.id = id
        self.expression = expression


class ChoiceInstructionIfElse(Node):
    def __init__(self, condition, if_instruction, else_instruction):
        self.condition = condition
        self.if_instruction = if_instruction
        self.else_instruction = else_instruction


class ChoiceInstructionIf(Node):
    def __init__(self, condition, if_instruction):
        self.condition = condition
        self.if_instruction = if_instruction


class WhileInstruction(Node):
    def __init__(self, condition, instruction):
        self.condition = condition
        self.instruction = instruction


class RepeatInstruction(Node):
    def __init__(self, instructions, condition):
        self.instructions = instructions
        self.condition = condition


class ReturnInstruction(Node):
    def __init__(self, expression):
        self.expression = expression


class ContinueInstruction(Node):
    pass


class BreakInstruction(Node):
    pass


class CompoundInstruction(Node):
    def __init__(self, declarations, instructions):
        self.declarations = declarations
        self.instructions = instructions


class Condition(Node):
    def __init__(self, expression):
        self.expression = expression


class Const(Node):
    def __init__(self, const):
        self.const = const

    def __str__(self):
        return str(self.const)


class Integer(Const):
    pass


class Float(Const):
    pass


class String(Const):
    pass


class Id(Const):
    pass


class BinaryExpression(Node):
    def __init__(self, left, op, right):
        self.left = left
        self.op = op
        self.right = right


class ParenthesisExpression(Node):
    def __init__(self, expression):
        self.expression = expression


class FunctionCall(Node):
    def __init__(self, id, instruction):
        self.id = id
        self.instruction = instruction
        

class Fundef(Node):
    def __init__(self, type, id, args_list, compound_instr):
        self.type = type
        self.id = id
        self.args_list = args_list
        self.compound_instr = compound_instr


class Argument(Node):
    def __init__(self, type, id):
        self.type = type
        self.id = id