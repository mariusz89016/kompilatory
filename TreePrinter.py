import AST


def addToClass(cls):
    def decorator(func):
        setattr(cls, func.__name__, func)
        return func

    return decorator


class TreePrinter:
    @addToClass(AST.Node)
    def printTree(self):
        raise Exception("printTree not defined in class " + self.__class__.__name__)

    @addToClass(AST.List)
    def printTree(self, indent):
        for obj in self.list:
            obj.printTree(indent)

    @addToClass(AST.Program)
    def printTree(self):
        for decl in self.declarations.list:
            decl.printTree(0)

        for fundef in self.fundefs.list:
            fundef.printTree(0)

        for instruction in self.instructions.list:
            instruction.printTree(0)

    @addToClass(AST.Declaration)
    def printTree(self, indent):
        print "%sDECL" % ("| "*indent, )
        self.inits.printTree(indent+1)

    @addToClass(AST.Init)
    def printTree(self, indent):
        print "%s=" % ("| "*indent, )
        self.id.printTree(indent+1)
        self.expression.printTree(indent+1)

    @addToClass(AST.PrintInstruction)
    def printTree(self, indent):
        print "%sPRINT" % ("| "*indent, )
        self.expression.printTree(indent+1)

    @addToClass(AST.LabeledInstruction)
    def printTree(self, indent):
        self.id.printTree(indent)
        self.instruction.printTree(indent+1)

    @addToClass(AST.Assignment)
    def printTree(self, indent):
        print "%s=" % ("| "*indent, )
        self.id.printTree(indent+1)
        self.expression.printTree(indent+1)

    @addToClass(AST.ChoiceInstructionIfElse)
    def printTree(self, indent):
        print "%sIF" % ("| "*indent, )
        self.condition.printTree(indent)
        self.if_instruction.printTree(indent+1)
        print "%sELSE" % ("| "*indent, )
        self.else_instruction.printTree(indent+1)

    @addToClass(AST.ChoiceInstructionIf)
    def printTree(self, indent):
        print "%sIF" % ("| "*indent, )
        self.condition.printTree(indent)
        self.if_instruction.printTree(indent+1)

    @addToClass(AST.WhileInstruction)
    def printTree(self, indent):
        print "%sWHILE" % ("| "*indent, )
        self.condition.printTree(indent)
        self.instruction.printTree(indent+1)

    @addToClass(AST.RepeatInstruction)
    def printTree(self, indent):
        print "%sREPEAT" % ("| "*indent, )
        self.instructions.printTree(indent+1)
        print "%sUNTIL" % ("| "*indent, )
        self.condition.printTree(indent+1)


    @addToClass(AST.ReturnInstruction)
    def printTree(self, indent):
        print "%sRETURN" % ("| "*indent, )
        self.expression.printTree(indent+1)

    @addToClass(AST.ContinueInstruction)
    def printTree(self, indent):
        print "%sCONTINUE" % ("| "*indent, )

    @addToClass(AST.BreakInstruction)
    def printTree(self, indent):
        print "%sBREAK" % ("| "*indent, )

    @addToClass(AST.CompoundInstruction)
    def printTree(self, indent):
        for declaration in self.declarations.list:
            declaration.printTree(indent+1)
        for instruction in self.instructions.list:
            instruction.printTree(indent)

    @addToClass(AST.Condition)
    def printTree(self, indent):
        self.expression.printTree(indent+1)

    @addToClass(AST.Const)
    def printTree(self, indent):
        print "%s%s" % ("| "*indent, self.const)

    @addToClass(AST.BinaryExpression)
    def printTree(self, indent):
        print "%s%s" % ("| "*indent, self.op)
        self.left.printTree(indent+1)
        self.right.printTree(indent+1)

    @addToClass(AST.ParenthesisExpression)
    def printTree(self, indent):
        self.expression.printTree(indent)

    @addToClass(AST.FunctionCall)
    def printTree(self, indent):
        print "%sFUNCALL" % ("| "*indent, )
        print "%s%s" % ("| "*(indent+1), self.id)
        self.instruction.printTree(indent+1)

    @addToClass(AST.Fundef)
    def printTree(self, indent):
        print "%sFUNDEF" % ("| "*indent, )
        print "%s%s" % ("| "*(indent+1), self.id)
        print "%sRET %s" % ("| "*(indent+1), self.type)
        for arg in self.args_list.list:
            arg.printTree(indent+1)
        self.compound_instr.printTree(indent+1)

    @addToClass(AST.Argument)
    def printTree(self, indent):
        print "%s%s %s" % ("| "*indent, "ARG", self.id)